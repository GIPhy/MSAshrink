/*
  ########################################################################################################

  MSAshrink: shrinking multiple sequence alignment
    
  Copyright (C) 2022  Institut Pasteur
  
  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU
  General Public License as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even
  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the  GNU General Public License along with this program.  If not, see
  <http://www.gnu.org/licenses/>.
  
  Contact:
   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr
   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr
   Bioinformatics and Biostatistics Hub                                 research.pasteur.fr/team/hub-giphy
   Institut Pasteur, Paris, FRANCE           research.pasteur.fr/team/bioinformatics-and-biostatistics-hub

  ########################################################################################################
*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Locale;
import java.util.stream.IntStream;

public class MSAshrink {

    //### constants   ###############################################################
    static final String VERSION = "2.0      Copyright (C) 2022 Institut Pasteur";
    static final String NOTHING = "N.o./.T.h.I.n.G";
    static final String STD_USL = "-?X";

    //### options   #################################################################
    static File    infile;         // -i
    static File    outfile;        // -o
    static String  usl;            // -u
    static double  cmax;           // -c
    static double  rmax;           // -r
    static boolean keepraw;        // -n
    static boolean opt;            // -m
    static boolean slink;          // -s
    static boolean verbose;        // -v
    static boolean verbose2;       // -V

    //### io   ######################################################################
    static String filename;
    static BufferedReader in;
    static BufferedWriter out;

    //### data   ####################################################################
    static BitSet busl;               // useless character (usl) byte codes
    static int n;                     // no. sequences
    static int m;                     // no. characters
    static ArrayList<String> lbl;     // labels
    static int lmax;                  // max label lgt
    static ArrayList<String> msa;     // msa
    static int[] cid;                 // col ids
    static BitSet ccmask;             // conserved cmask (i.e. with no usl)
    static int nccol;                 // no. conserved cols (i.e. with no usl)
    static int[] ciid;                // incomplete col ids (i.e. with at least one usl)
    static int[] rid;                 // row ids
    static BitSet[] cu;               // usl presence per col
    static BitSet[] ru;               // usl presence per row
    static int[] rcpt;                // no. usl per row
    static BitSet cmaskopt;           // optimal cmask
    static BitSet rmaskopt;           // optimal rmask
    static double scoreopt;           // optimal score

    //### stuffs   ##################################################################
    static int c, o, r, x, y;
    static int vcpt, nry, ncol, nrow, nusl, rusl, cutoff;
    static long ox, nusll, jo, nusllo, nuslopt;
    static double score, min, scoreo;
    static byte[] bseq;
    static int[] rthr, csid, ccpt;
    static int[][] idcu, idcpt;
    static double[] clustscore;
    static double[][] dm;
    static BitSet bs, bsi, bsj, curclust, cmask, rmask;
    static BitSet[] rclust;
    static String line, seq;
    static StringBuilder sb;

    public static void main(String[] args) throws IOException {

	if ( args.length < 4 ) { usage(); System.exit(1); }

	//#############################################################################################################
	//#############################################################################################################
	//### reading options                                                                                       ###
	//#############################################################################################################
	//#############################################################################################################
	infile = new File(NOTHING);  // -i
	outfile = new File(NOTHING); // -o
	usl = STD_USL;               // -u
	cmax = 0.5;                  // -c
	rmax = 0.5;                  // -r
	keepraw = false;             // -n
	opt = false;                 // -m
	verbose = false;             // -v
	verbose2 = false;            // -V
	slink = true;                // -w
	o = -1;
	while ( ++o < args.length ) {
	    if ( args[o].equals("-i") )     {   infile = new File(args[++o]);           continue; }
	    if ( args[o].equals("-o") )     {  outfile = new File(args[++o]);           continue; }
	    if ( args[o].equals("-u") )     {      usl = args[++o];                     continue; }
	    if ( args[o].equals("-c") ) try {     cmax = Double.parseDouble(args[++o]); continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -c)"); System.exit(1); }
	    if ( args[o].equals("-r") ) try {     rmax = Double.parseDouble(args[++o]); continue; } catch ( NumberFormatException e ) { System.err.println("incorrect value: " + args[o] + " (option -r)"); System.exit(1); }
	    if ( args[o].equals("-n") )     {  keepraw = true;                          continue; }
	    if ( args[o].equals("-m") )     {      opt = true;                          continue; }
	    if ( args[o].equals("-v") )     {  verbose = true;                          continue; }
	    if ( args[o].equals("-V") )     { verbose2 = verbose = true;                continue; }
	    if ( args[o].equals("-h") )     {  usage();                           System.exit(0); }
	    if ( args[o].equals("-w") )     {    slink = false;                         continue; }
	}

	//### infile   ###################################################################
	filename = infile.toString();
	if ( filename.equals(NOTHING) )     { System.err.println("no input file (option -i)");                        System.exit(1); }
	if ( ! infile.exists() )            { System.err.println("infile does not exist (option -i): " + filename);   System.exit(1); }

	//### outfile   ##################################################################
	filename = outfile.toString();
	if ( filename.equals(NOTHING) )     { System.err.println("no output file (option -o)");                       System.exit(1); }

	//### unknown characters   #######################################################
	busl = new BitSet(Byte.MAX_VALUE);
	for (byte b: usl.getBytes(StandardCharsets.UTF_8)) busl.set(b);

	
	//#############################################################################################################
	//#############################################################################################################
	//### reading infile                                                                                        ###
	//#############################################################################################################
	//#############################################################################################################
	if ( verbose ) System.out.print("reading MSA ...");
	in = Files.newBufferedReader(Path.of(infile.toString()));
	lbl = new ArrayList<String>();
	msa = new ArrayList<String>();
	sb = new StringBuilder("");
	m = lmax = vcpt = 0;
	while ( true ) {
	    try { line = in.readLine().trim(); }  catch ( NullPointerException e ) { in.close(); break; }
	    if ( line.startsWith(">") ) {
		if ( verbose && (++vcpt) % 50 == 0 ) System.out.print(".");
		if ( sb.length() > 0 ) { 
		    seq = sb.toString().replaceAll(" ", "");
		    if ( m > 0 && seq.length() != m ) { System.err.println("problem with input file: different sequence lengths"); System.exit(1); }
		    m = seq.length();
		    msa.add(seq);
		    sb = new StringBuilder(m);
		}
		lbl.add(line);
		lmax = ( lmax < line.length() ) ? line.length() : lmax;
		continue;
	    }
	    sb = sb.append(line);
	}
	seq = sb.toString().replaceAll(" ", "");
	if ( m > 0 && seq.length() != m )             { System.err.println("problem with input file: different sequence length"); System.exit(1); }
	msa.add(seq);
	if ( verbose ) System.out.println(" [ok]");

	n = msa.size();
	cid = IntStream.range(0, m).toArray();
	rid = IntStream.range(0, n).toArray();

	
	//#############################################################################################################
	//#############################################################################################################
	//### processing MSA                                                                                        ###
	//#############################################################################################################
	//#############################################################################################################
	if ( verbose ) System.out.print("processing MSA ");
	cu = new BitSet[m]; for (int j: cid) cu[j] = new BitSet(n);
	ru = new BitSet[n];
	rcpt = new int[n];
	bsi = new BitSet(m);
	cutoff = (int) (( 0 < rmax && rmax < 1 ) ? Math.floor(rmax*m) : rmax); //## NOTE: maximum allowed no. usl per row
	vcpt = n/10; // verbose
	for (int i: rid) {
	    if ( verbose && i >= vcpt ) { System.out.print("."); vcpt += n/10; }
	    bseq = msa.get(i).getBytes(StandardCharsets.UTF_8);
	    bsi.clear(0, m);
	    for (int j: cid) if ( busl.get(bseq[j]) ) { cu[j].set(i); bsi.set(j); } //## NOTE: == true when msa[i][j] == usl
	    ru[i] = bsi.get(0, m);
	    rcpt[i] = bsi.cardinality(); //## NOTE: == no. usl in row i
	}
	if ( verbose ) System.out.println(" [ok]");

	ccmask = new BitSet(m);
	nusll = 0;
	for (int j: cid) if ( (c=cu[j].cardinality()) != 0 ) { ccmask.set(j); nusll += c; }
	ciid = ccmask.stream().toArray(); //## NOTE: incomplete col ids
	ccmask.flip(0, m);                //## NOTE: ccmask.get(j) == true when col j is complete (i.e. no usl); TIP: such cols are inside every possible MSA shrink
	nccol = ccmask.cardinality();     //## NOTE: nccol == no. complete cols

	ox = n; ox *= m;
	System.out.println(String.format(Locale.US, "nrow=%-" + String.valueOf(n).length()
					 +       "s  ncol=%-" + String.valueOf(m).length()
					 +       "s  nusl=%-" + String.valueOf(ox).length()
					 +       "s", n, m, nusll));


	//#############################################################################################################
	//#############################################################################################################
	//### col filtering only                                                                                    ###
	//#############################################################################################################
	//#############################################################################################################
	if ( keepraw ) {
	    //### filtering cols
	    if ( verbose ) System.out.print("shrinking MSA ");
	    cmask = ccmask.get(0, m);
	    cutoff = (int) (( 0 < cmax && cmax < 1 ) ? Math.floor(cmax*n) : cmax); //## NOTE: maximum allowed no. usl per col
	    vcpt = m/10; // verbose
	    for (int j: ciid) {
		if ( verbose && j >= vcpt ) { System.out.print("."); vcpt += m/10; }
		if ( cu[j].cardinality() <= cutoff ) cmask.set(j); else nusll -= cu[j].cardinality();
	    }
	    ncol = cmask.cardinality();
	    if ( verbose ) System.out.println(" [ok]");

	    ox = n; ox *= ncol;
	    System.out.println(String.format(Locale.US, "nrow=%-" + String.valueOf(n).length()
					     +       "s  ncol=%-" + String.valueOf(m).length()
					     +       "s  nusl=%-" + String.valueOf(ox).length()
					     +       "s", n, ncol, nusll));
	    
	    //### writing outfile
	    if ( verbose ) System.out.print("writing outfile ");
	    vcpt = n/10; // verbose
	    filename = outfile.toString();
	    out = Files.newBufferedWriter(Path.of(filename));
	    for (int i: rid) {
		if ( verbose && i >= vcpt ) { System.out.print("."); vcpt += n/10; }
		out.write(lbl.get(i));
		out.newLine();
		bseq = msa.get(i).getBytes(StandardCharsets.UTF_8);
		for (int j: cmask.stream().toArray()) out.write((char) bseq[j]);
		out.newLine();
	    }
	    if ( verbose ) System.out.println(" [ok]");
	    out.close();
	    
	    System.exit(0);
	}
	

	//#############################################################################################################
	//#############################################################################################################
	//### computing pairwise overlap between each row pair                                                      ###
	//#############################################################################################################
	//#############################################################################################################
	if ( verbose ) System.out.print("comparing sequences ");
	dm = new double[n][];
	vcpt = n/10; // verbose
	for (int i: rid) {
	    if ( verbose && i >= vcpt ) { System.out.print("."); vcpt += n/10; }
	    dm[i] = new double[i];
	    bsi = ru[i].get(0, m);
	    for (int j: rid) 
		if ( j < i ) {
		    bs = ru[j].get(0, m);
		    bs.or(bsi);
		    dm[i][j] = bs.cardinality() / (double) m;
		}
	}
	if ( verbose ) System.out.println(" [ok]");

	
	//#############################################################################################################
	//#############################################################################################################
	//### searching optimal shrinking                                                                           ###
	//#############################################################################################################
	//#############################################################################################################
	if ( verbose ) System.out.println("searching optimal skrinking ...");
	cmask  = new BitSet(m);
	ccpt   = new int[m];   
	idcu   = new int[m-nccol][2];
	ox = n; ox *= m;

	//## NOTE: Given a shrinked MSA of dimension nrow x ncol containing nusll usl,
	//          + the no. non-usl per row is: (nrow * ncol - nusll) / nrow
	//          + the no. non-usl per col is: (nrow * ncol - nusll) / ncol
	//         The score of a shrinked MSA is therefore the genometric mean of these two values, i.e.
	//          + score := (nrow * ncol - nusll) (nrow * ncol)^-0.5
	//## NOTE: scoreopt == max shrinked MSA score 
	scoreopt = 0;

	//## NOTE: rthr[j] == max allowed no. usl for pos. j of a row (according to rmax)
	rthr = new int[m+1]; for (int j: cid) rthr[j] = (int) (( 0 < rmax && rmax < 1 ) ? Math.floor(rmax*j) : rmax);
	rthr[m] = (int) (( 0 < rmax && rmax < 1 ) ? Math.floor(rmax*m) : rmax);

	//## NOTE: curclust.stream().toArray() == ids of the active row clusters
	curclust = new BitSet(n); curclust.set(0, n);

	//## NOTE: clustscore[i] == score of the active row cluster i
	clustscore = new double[n]; for (int i: rid) clustscore[i] = (m - ru[i].cardinality()) / Math.sqrt(m);

	//## NOTE: rclust.get(x).stream().toArray() == row ids in the (active) row cluster x
	rclust = new BitSet[n]; for (int i: rid) { rclust[i] = new BitSet(n); rclust[i].set(i); }
	
	while ( curclust.cardinality() > 1 ) {
	    //## searching for row clusters x and y to agglomerate
	    x = y = -1; min = Double.POSITIVE_INFINITY;
	    for (int i: curclust.stream().toArray())
		for (int j: curclust.stream().toArray())
		    if ( j < i ) min = ( dm[i][j] < min ) ? dm[x=i][y=j] : min;
	    
	    //## updating dissimilarities between parent(x,y) and every i != x,y
	    for (int i: curclust.stream().toArray())
		if ( i != x && i != y )
		    dm[(i<x)?x:i][(i<x)?i:x] = ( slink )
			? ( dm[(i<y)?y:i][(i<y)?i:y] < dm[(i<x)?x:i][(i<x)?i:x] ) ? dm[(i<y)?y:i][(i<y)?i:y] : dm[(i<x)?x:i][(i<x)?i:x]                                                 //## NOTE: Slink
			: (rclust[x].cardinality()*dm[(i<x)?x:i][(i<x)?i:x] + rclust[y].cardinality()*dm[(i<y)?y:i][(i<y)?i:y]) / (rclust[x].cardinality() + rclust[y].cardinality());  //## NOTE: UPGMA
	    
	    //## swapping x and y so that row cluster x has the greatest score (if any)
	    if ( clustscore[x] < clustscore[y] ) { x ^= y; y ^= x; x ^= y; /*o = x; x = y; y = o;*/ }

	    //## sorting rows in cluster y (cluster y has the smallest score)
	    nry = rclust[y].cardinality();
	    idcpt = new int[nry][2];
	    r = -1;
	    for (int i: rclust[y].stream().toArray()) {
		++r;
		idcpt[r][0] = i;
		idcpt[r][1] = rcpt[i];
	    }
	    Arrays.sort(idcpt, (a, b) -> -Integer.compare(a[1], b[1]));
	    //## NOTE: idcpt[][1] == sorted no. usl per row in cluster y 
	    //## NOTE: idcpt[][0] == corresponding sorted row ids
	    	    
	    //## merging x and y, and discarding y
	    rmask = rclust[x].get(0, n);
	    rclust[x].or(rclust[y]);
	    curclust.clear(y);
	    clustscore[y] = -1;

	    //## searching the best skrinking by adding each sorted row (from cluster y) to the row cluster x
	    r = -1;
	    while ( ++r < nry ) {
		rmask.set(idcpt[r][0]);      //## NOTE: current row ids
		nrow = rmask.cardinality();  //## NOTE: current no. rows

		score = nrow; score *= m; score = Math.sqrt(score); //## NOTE: here 'score' is the greatest possible score value with the current rows
		if ( score < scoreopt && r < nry-1 ) {              //## NOTE: if score < scoreopt AND if the current row cluster is not the largest one (i.e. r != nry-1),
		    if ( verbose ) {                                //         then no need to compute the optimal shrinking for this row subset
			if ( verbose2 ) {
			    for (int i: rid) System.out.print( ((rmask.get(i))?"1":"0") );
			    System.out.print("  ");
			}
			line = String.format(Locale.US, "nusl=%-" + String.valueOf(ox).length() + "s", ox);
			System.out.println(String.format(Locale.US, "nrow=%-" + String.valueOf(n).length()
							 +       "s  ncol=%-" + String.valueOf(m).length()
							 +       "s  " + " ".repeat(line.length())
							 +       "  score<%.3f", nrow, ncol, scoreopt));
		    }
		    continue;
		}

		//## sorting incomplete cols
		cutoff = (int) (( 0 < cmax && cmax < 1 ) ? Math.floor(cmax*nrow) : cmax); //## NOTE: max allowed no. usl per col (according to cmax)
		ncol = 0;
		c = -1;
 		for (int j: ciid) {
		    bs = cu[j].get(0, n);
		    bs.and(rmask);
		    nusl = bs.cardinality();
		    ++c;
		    idcu[c][0] = j;
		    idcu[c][1] = ccpt[j] = nusl;
		    ncol = ( nusl == nrow ) ? ncol : ( opt || nusl <= cutoff ) ? ++ncol : ncol;
		}
		Arrays.sort(idcu, (a, b) -> Integer.compare(a[1], b[1]));
		//## NOTE: ccpt[j]   == no. usl in col j
		//## NOTE: ncol      == no. incomplete cols containing <= cutoff usl
		//## NOTE: idcu[][1] == sorted no. usl per incomplete col 
		//## NOTE: idcu[][0] == corresponding sorted col ids (TIPS: the 'ncol' first ids are sufficient)
		csid = new int[ncol];   //## NOTE: sorted incomplete col id subset
		c = ncol; while ( --c >= 0 ) csid[c] = idcu[c][0];

		//## opt: searching for the col subset that maximizes the score ###############
		if ( opt ) {
		    //## parsing cols from csid to find optimal score
		    scoreo = scoreopt-1; //## NOTE: scoreo is the best score obtained with the current rmask
		    ncol = nccol;
		    nusll = 0;           //## NOTE: nusll == total no. usl in the current rmask x cmask selection
		    for (int j: csid) {
			++ncol;
			nusll += ccpt[j];
			score = nrow; score *= ncol; score = ( score > 0 ) ? (score - nusll) / Math.sqrt(score) : 0;
			if ( score >= scoreo ) { //## NOTE: local optimum (for the current rmask)
			    scoreo = score;
			    jo = j;
			    nusllo = nusll;
			}
		    }
		    if ( scoreo >= scoreopt ) { //## NOTE: the local optimum is better than the global one
			//## first: keeping the conserved cols
			cmask = ccmask.get(0, m);
			//## second: setting the optimal col
			for (int j: csid) {
			    cmask.set(j);
			    if ( j == jo ) break;
			}
			ncol  = cmask.cardinality();
			nusll = nusllo;
		    }
		}

		//## ! opt: searching for the col subset that verifies the constraints ########
		else {
		    //## first: keeping the conserved cols
		    cmask = ccmask.get(0, m);
		    //## second: keeping cols from csid
		    nusll = 0;        //## NOTE: nusll == total no. usl in the current rmask x cmask selection
		    for (int j: csid) {
			cmask.set(j);
			nusll += ccpt[j];
		    }
		    //## third: clearing cols causing too many usl within the current row subset (according to rmax)
		    for (int i: rmask.stream().toArray()) {
			bsi = ru[i].get(0, m);
			ncol = nccol;
			rusl = 0;
			for (int j: csid) {
			    ++ncol;
			    rusl = ( bsi.get(j) ) ? ++rusl : rusl;   //## NOTE: rusl = no. usl in the 'ncol' first 'csid' cols of row i
			    if ( cmask.get(j) && rusl > rthr[ncol] ) {
				cmask.clear(j);
				nusll -= ccpt[j];
			    }
			}
		    }
		    ncol = cmask.cardinality();
		}
		
		//## computing score of the shrinked MSA corresponding to the current set of rows
		score = nrow; score *= ncol; score = ( score > 0 ) ? (score - nusll) / Math.sqrt(score) : 0;
		
		if ( verbose ) {
		    if ( verbose2 ) {
			for (int i: rid) System.out.print( ((rmask.get(i))?"1":"0") );
			System.out.print("  ");
		    }
		    System.out.print(String.format(Locale.US, "nrow=%-" + String.valueOf(n).length()
						   +       "s  ncol=%-" + String.valueOf(m).length()
						   +       "s  nusl=%-" + String.valueOf(ox).length()
						   +       "s  score=%.3f", nrow, ncol, nusll, score));
		}
		
		if ( score >= scoreopt ) {
		    scoreopt = score;
		    cmaskopt = cmask.get(0, m);
		    rmaskopt = rmask.get(0, n);
		    nuslopt = nusll;
		    if ( verbose ) System.out.println("  *");
		    continue;
		}
		if ( verbose ) System.out.println("");
	    }
	    
	    clustscore[x] = score; //## NOTE: score of the shrinked MSA corresponding to the union of the row clusters x and y
	}
	if ( verbose ) System.out.println("... [ok]");
	
	ncol = cmaskopt.cardinality();
	nrow = rmaskopt.cardinality();
	System.out.println(String.format(Locale.US, "nrow=%-" + String.valueOf(n).length()
					 +       "s  ncol=%-" + String.valueOf(m).length()
					 +       "s  nusl=%-" + String.valueOf(ox).length()
					 +       "s", nrow, ncol, nuslopt));

	
	//#############################################################################################################
	//#############################################################################################################
	//### writing outfile                                                                                       ###
	//#############################################################################################################
	//#############################################################################################################
	if ( verbose ) System.out.print("writing outfile ");
	vcpt = nrow/10; // verbose
	filename = outfile.toString();
	out = Files.newBufferedWriter(Path.of(filename));
	for (int i: rmaskopt.stream().toArray()) {
	    if ( verbose && i >= vcpt ) { System.out.print("."); vcpt += nrow/10; }
	    out.write(lbl.get(i));
	    out.newLine();
	    //seq = msa.get(i); for (int j: cmask.stream().toArray()) out.write(seq.charAt(j));
	    bseq = msa.get(i).getBytes(StandardCharsets.UTF_8);
	    for (int j: cmaskopt.stream().toArray()) out.write((char) bseq[j]);
	    out.newLine();
	}
	out.close();
	if ( verbose ) System.out.println(" [ok]");
	
    }

    static void usage() {
	System.out.println("");
	System.out.println(" MSAshrink v" + VERSION);
	System.out.println("");
	System.out.println(" USAGE:  MSAshrink  -i <infile>  -o <outfile>  [options]");
	System.out.println("");
	System.out.println(" OPTIONS:");
	System.out.println("   -i <file>    input FASTA file containing a multiple sequence alignment (mandatory)");
	System.out.println("   -o <file>    output file name (mandatory)");
	System.out.println("   -u <string>  useless residues/character states (case sensitive; default: \"" + STD_USL + "\")");
	System.out.println("   -c <real>    column cutoff;  when c < 1  or  c >= 1,  the proportion  or  number of");
	System.out.println("                useless  residues   per  aligned   character  should   not  exceed  c,");
	System.out.println("                respectively (default: 0.5)");
	System.out.println("   -r <real>    row cutoff;  when r < 1 or r >= 1, the proportion or number of useless");
	System.out.println("                residues per sequence should not exceed r, respectively (default: 0.5)");
	System.out.println("   -m           searches for both the row and column subsets that maximizes the useful");
	System.out.println("                residue content (default: not set)");
	System.out.println("   -n           no sequence removal: equivalent to -r 0.99999 (default: not set)");
	System.out.println("   -v           verbose mode (default: not set)");
	System.out.println("   -V           more verbose (default: not set)");
	System.out.println("   -h           prints this help and exit");
	System.out.println("");
    }
    
}
