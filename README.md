[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/)
[![JAVA](https://img.shields.io/badge/Java-11-be0032?logo=java)](http://www.oracle.com/technetwork/java/javase/downloads/index.html)


# MSAshrink


_MSAshrink_ is a command line program written in [Java](https://docs.oracle.com/en/java/) to shrink a multiple sequence alignment, that is, selecting both the largest subsets of sequences and characters that verify specified maximum contents of useless residues/character states (e.g. gaps) per row and column. _MSAshrink_ is also able to search for the row and column subsets that maximize the useful (i.e. non-useless) residues/character state content. As the MSA shrinking problem is likely NP-hard (see Method below), _MSAskrink_ implements a quite fast method that often provides optimal sequence/character subsets, but not always (depending on the distribution of the useless residues).



## Compilation and execution

The source code of _MSAshrink_ is inside the _src_ directory and could be compiled and executed in two different ways.

#### Building an executable jar file

Clone this repository with the following command line:

```bash
git clone https://gitlab.pasteur.fr/GIPhy/MSAshrink.git
```

On computers with [Oracle JDK](http://www.oracle.com/technetwork/java/javase/downloads/index.html) (**11** or higher) installed, a Java executable jar file can be created. In a command-line window, go to the _src_ directory and type:

```bash
javac MSAshrink.java 
echo Main-Class: MSAshrink > MANIFEST.MF 
jar -cmvf MANIFEST.MF MSAshrink.jar MSAshrink.class
rm MANIFEST.MF MSAshrink.class
```

This will create the executable jar file `MSAshrink.jar` that can be run with the following command line model:

```bash
java -jar MSAshrink.jar [options]
```


#### Building a native code binary

Clone this repository with the following command line:

```bash
git clone https://gitlab.pasteur.fr/GIPhy/MSAshrink.git
```

On computers with [GraalVM](https://www.graalvm.org/downloads/) installed, a native executable can be built. In a command-line window, go to the _src_ directory, and type:

```bash
javac MSAshrink.java 
native-image MSAshrink MSAshrink
rm MSAshrink.class
```

This will create the native executable `MSAshrink` that can be run with the following command line model:

```bash
./MSAshrink [options]
```


## Usage

Run _MSAshrink_ without option to read to following documentation:

```
 USAGE:  MSAshrink  -i <infile>  -o <outfile>  [options]

 OPTIONS:
   -i <file>    input FASTA file containing a multiple sequence alignment (mandatory)
   -o <file>    output file name (mandatory)
   -u <string>  useless residues/character states (case sensitive; default: "-?X")
   -c <real>    column cutoff;  when c < 1  or  c >= 1,  the proportion  or  number of
                useless  residues   per  aligned   character  should   not  exceed  c,
                respectively (default: 0.5)
   -r <real>    row cutoff;  when r < 1 or r >= 1, the proportion or number of useless
                residues per sequence should not exceed r, respectively (default: 0.5)
   -m           searches for both the row and column subsets that maximizes the useful
                residue content (default: not set)
   -n           no sequence removal: equivalent to -r 0.99999 (default: not set)
   -v           verbose mode (default: not set)
   -V           more verbose (default: not set)
   -h           prints this help and exit
```


## Notes

* The input file (option `-i`) should be in FASTA format and contains only UTF-8 characters. Any kind of residues/character states is accepted (e.g. nucleotides, amino-acid), as the aim of _MSAshrink_ is to only filter out sequences and/or aligned character containing too many useless residues/character states.

* Of important note, the default set of useless residues/character states is: `-?X`, which is dedicated to current usage when dealing with amino-acid multiple sequence alignments. It is therefore recommanded to complete this default set when dealing with nucleotide data, i.e. `-u "-?XN"`. Anyhow, any other set of useless residues/character states can be specified without impact on the overall running times, e.g. `-u "WSKRYBDHV-?XN"`

* The maximum allowed content of useless residues/character states per row (sequence) and column (aligned character) can be set using options `-r` and `-c`, respectively. When a specified values is &geq;&nbsp;1, _MSAshrink_ interprets it as the maximum allowed  **number** of useless residues/character states. When a specified value is <&nbsp;1, _MSAshrink_ interprets it as the maximum allowed **proportion** of useless residues/character states.

* To only filter out columns containing too many useless residues/character states (as set using option `-c`) without discarding any row (i.e. `-r 0.9999999`), the option `-n` is recommanded.

* An alternative option (`-m`) can be set to search for the subsets of rows and columns that maximize the useful residues/character state content. This last option is not compatible with options `-c` and `-r`.

## Method


### Background

_MSAshrink_ implements an original approach to solve the problem of shrinking a multiple sequence alignment (MSA). A MSA is a _n_&nbsp;&times;&nbsp;_m_ matrix containing residues, some being useful (e.g. nucleotide or amino acid character states), other being useless (e.g. gaps, unknown character states). The MSA shrinking problem is to find the subset(s) of _r_ (&leq;&nbsp;_n_) rows and _c_ (&leq;&nbsp;_m_) columns that contain(s) the maximum numbers of useful residues per row and column, while verifying specified maximum rate cutoffs of useless residue content per row and column.

A trivial approach is to first filter out each column and next each row that does not verify the column/row cutoffs, respectively (or, alternatively, first filter out the rows, and next the columns). However, such an approach often yields an unacceptably small row/column subsets. For example, when the two row and column cutoffs are 0 (i.e. no useless residue allowed), this trivial approach discards everything when the MSA contains at least one useless residue in each row and column. It is therefore required to consider different combinations of rows and columns. However, for a _n_&nbsp;&times;&nbsp;_m_ matrix, there is a total of (2<sup>_n_</sup>&nbsp;&minus;&nbsp;1)(2<sup>_m_</sup>&nbsp;&minus;&nbsp;1) different combinations, therefore requiring intractable running times.

Moreover, the MSA shrinking problem is related to an NP-hard one. Let _w_<sub>_ij_</sub>&nbsp;=&nbsp;1 when the _j_<sup>th</sup> residue in row _i_ is useless (0 otherwise), and _u_<sub>_j_</sub>&nbsp;=&nbsp;&Sigma;<sub>_i_</sub>&nbsp;_w_<sub>_ij_</sub> be the number of useless residue(s) in column _j_. When the aim is to shrink only the columns of a _n_&nbsp;&times;&nbsp;_m_ matrix to ensure a maximum number of _k_&nbsp;&geq;&nbsp;1 useless residues per row, one has to find the _m_ values _x_<sub>_j_</sub>&nbsp;&in;&nbsp;{0,&nbsp;1} that maximize the average useful residue content per column &Sigma;<sub>_j_</sub>&nbsp;_x_<sub>_j_</sub>&nbsp;(_n_&nbsp;&minus;&nbsp;_u_<sub>_j_</sub>) while verifying the _n_ constraints &Sigma;<sub>_j_</sub>&nbsp;_x_<sub>_j_</sub>&nbsp;_w_<sub>_ij_</sub>&nbsp;&leq;&nbsp;_k_ (_i_&nbsp;=&nbsp;1,&nbsp;2,&nbsp;...,&nbsp;_n_). Such a problem is known as the (0/1) Multidimensional Knapsack Problem (MKP), which is NP-hard (e.g. Weingartner and Ness 1967, Chu and Beasley 1998, Laabadi et al. 2018). Therefore, as the MSA shrinking problem is a combination of two MKP (i.e. for rows and columns), it is required to implement heuristics to quickly approximate the optimal solution.


### Algorithm details

#### Fixed cutoffs

To cope with the above observations, _MSAshrink_ implements two complementary heuristics, as well as a criterion that determines the optimal row/column subsets. The first heuristics builds a suited column subset (verifying both cutoffs) from a fixed row subset. The second heuristics generates a reduced list of astute row subsets.

_**Criterion**_<br>
Given two fixed subsets of _r_ rows and _c_ columns, _MSAshrink_ quantifies their overall useful residue content by computing a score _s_ based on both the average useful residue contents per row and column. More formally, denoting _u_ the number of useless residues in a _r_&nbsp;&times;&nbsp;_c_ (sub)matrix, the average numbers of useful residues per row and column are (_r_&nbsp;_c_&nbsp;&minus;&nbsp;_u_)&nbsp;/&nbsp;_r_ and (_r_&nbsp;_c_&nbsp;&minus;&nbsp;_u_)&nbsp;/&nbsp;_c_, respectively. The score _s_ is the geometric mean of these two values, i.e. _s_&nbsp;=&nbsp;(_r_&nbsp;_c_&nbsp;&minus;&nbsp;_u_)&nbsp;(_r_&nbsp;_c_)<sup>&minus;0.5</sup>. The score _s_ is as large as the selected rows and columns are large and contain few useless residues on average.

_**Heuristics 1**_<br>
Given a fixed subset of _r_ rows, _MSAshrink_ determines its corresponding subset of columns in two successive steps:<br>
&emsp;1. selecting the _c'_ columns that verify the column cutoff,<br>
&emsp;2. sorting the _c'_ columns according to their decreasing number of useless residues (within the _r_ rows),<br>
&emsp;3. selecting the largest subset of _c_ (&leq;&nbsp;_c'_) sorted columns that enables to verify the row cutoff for each of the _r_ row.<br>
For a _r_&nbsp;&times;&nbsp;_m_ matrix, these three steps require _O_(_m_(_r_&nbsp;+&nbsp;log&nbsp;_m_)) time complexity.

_**Heuristics 2**_<br>
To generate row subsets, _MSAshrink_ performs a hierarchical clustering (single linkage criterion) of the rows from their pairwise overlapping useful residue contents, therefore leading to at least _n_&nbsp;&minus;&nbsp;1 subsets of similar rows. However, when two row subsets should be clustered, _MSAshrink_ processes them following three other steps:<br>
&emsp;a. selecting the row subset _R_ (and the corresponding column subset; see above) that yields the greatest score _s_,<br>
&emsp;b. sorting the rows from the other subset _R'_ according to their decreasing number of useless residues (within the _m_ columns),<br>
&emsp;c. sucessively adding the sorted rows from _R'_ into _R_.<br>
For each pair of row subsets _R_ and _R'_ of size _r_ and _r'_, respectively, these steps therefore enable to generate _r'_ new row subsets of size _r_&nbsp;+&nbsp;1, _r_&nbsp;+&nbsp;2, ..., _r_&nbsp;+&nbsp;_r'_. For a _n_&nbsp;&times;&nbsp;_m_ matrix, this last heuristics can therefore generate up to _n_<sup>2</sup> different row subsets.

Thanks to the second heuristics, different subsets of rows with comparable useful contents are generated. For each of these row subsets, _MSAshrink_ determines the corresponding column subset using the first heuristics, and computes the associated score _s_. Finally, _MSAshrink_ returns the shrinked MSA corresponding to the row/column subset pair associated to the maximum value of _s_.

#### Maximal useful content

An alternative strategy is to search for the row and column subsets that maximize the score _s_. _MSAshrink_ implements this strategy using a slight modification of the first heuristics.

_**Heuristics 1b**_<br>
Given a fixed subset of _r_ rows, _MSAshrink_ determines its optimal subset of columns in three successive steps:<br>
&emsp;1. sorting the _c'_ columns according to their decreasing number of useless residues (within the _r_ rows),<br>
&emsp;2. selecting the largest subset of _c_ (&leq;&nbsp;_c'_) sorted columns that maximizes the score _s_.<br>
For a _r_&nbsp;&times;&nbsp;_m_ matrix, these two steps require _O_(_m_(_r_&nbsp;+&nbsp;log&nbsp;_m_)) time complexity.

When searching for the row and column subsets that maximizes the useful residue content, _MSAshrink_ generates row subsets using the second heuristics. For each of these row subsets, _MSAshrink_ determines its corresponding column subset using the heuristics 1b, and computes the associated score _s_. Finally, _MSAshrink_ returns the shrinked MSA corresponding to the row and column subsets that led to the maximum value of _s_.



## Examples

The directory _example/_ contains the FASTA-formatted file _msa.fa_ containing the following multiple nucleotide sequence alignement:

```
0 ACNGTACGTACTGTACGTAATGTAC-----A-----------------------------------------------------------CCAGTGC----
1 ACNGTACGTACTG-ACGTAATGTACC----A-----------------------------------------------------------CCAGTGC----
2 ACNGTACGTAC---AC--AATGTACCC---A-----------------------------------------------------------CCAG-------
3 ACNGTACGTACTGTACGTAATGTACCCC--A-----------------------------------------------------------CCAGTGC----
4 --------TACTGT---------ACCCCC-A-----------------------------------------------------------CCAGTGCTAGT
5 ACNGTA-----------T????TACCCCCCA---------------------------------------------ATAA----------CCAG-------
6 ----------------------TACCCCCCATATATATATATATATATATATATATATATATATATATATATATATATAATATATATTTTCCAGTGCTAGT
7 NNNNNNNNNNCTG------------------------TATATATATATATATATATATATATATATATATATATATATAA------TTTT-------TAGT
8 ACNGTANNNNCNNNNNNTAATGTACCCCCCA--------------------------------------------TATAATATATATTTTCCAGTGCTAGT
```

<br> 

The following command line runs _MSAshink_ with default cutoffs, i.e. no more than 50% useless residues (`"-?XN"`) per row and column:

```bash
MSAshrink  -i msa.fa  -o msa.shrink.fa  -u "-?XN"
```

The resulting shrinked MSA (7 rows, 33 columns) is written into the output file _msa.shrink.fa_:

```
0 ACGTACGTACTGACTAATGTAC---ACCAGTGC
1 ACGTACGTACTGACTAATGTACC--ACCAGTGC
2 ACGTACGTAC--AC-AATGTACCC-ACCAG---
3 ACGTACGTACTGACTAATGTACCCCACCAGTGC
4 -------TACTG--------ACCCCACCAGTGC
5 ACGTA---------T????TACCCCACCAG---
8 ACGTANNNNCNNNNTAATGTACCCCACCAGTGC
```

<br>

Other shrinked MSA can be obtained from the same example by setting different pairs of cutoff values, as illustrated below:


```bash
MSAshrink  -i msa.fa  -o msa.shrink.fa  -u "-?XN"  -r 0.1  -c 0.1
```

```
6 TATATATATATATATATATATATATATATATATATATATATAATTTTTAGT
7 TATATATATATATATATATATATATATATATATATATATATAATTTTTAGT
```

<br>

```bash
MSAshrink  -i msa.fa  -o msa.shrink.fa  -u "-?XN"  -r 0.3  -c 0.3
```

```
0 ACGTACGTACTGACGTAATGTAC-ACCAGTGC
1 ACGTACGTACTGACGTAATGTACCACCAGTGC
2 ACGTACGTAC--AC--AATGTACCACCAG---
3 ACGTACGTACTGACGTAATGTACCACCAGTGC
```

<br>

```bash
MSAshrink  -i msa.fa  -o msa.shrink.fa  -u "-?XN"  -r 0.9  -c 0.9
```

```
6 -------------TACCCCCCATATATATATATATATATATATATATATATATATATATATATATATATAATATATATTTTCCAGTGCTAGT
7 NNNNNCTG--------------------TATATATATATATATATATATATATATATATATATATATATAA------TTTT-------TAGT
8 ACGTACNNTAATGTACCCCCCA--------------------------------------------TATAATATATATTTTCCAGTGCTAGT
```

<br>

```bash
MSAshrink  -i msa.fa  -o msa.shrink.fa  -u "-?XN"  -r 0.6  -c 0.4
```

```
0 ACGTATACTAATGTAC--ACCAGTGC
1 ACGTATACTAATGTACC-ACCAGTGC
2 ACGTATAC-AATGTACCCACCAG---
3 ACGTATACTAATGTACCCACCAGTGC
4 -----TAC------ACCCACCAGTGC
5 ACGTA---T????TACCCACCAG---
8 ACGTANNCTAATGTACCCACCAGTGC
```

<br>

Finally, the following command line runs _MSAshink_ to search for the row and column subsets that maximize the useful residue content:

```bash
MSAshrink  -i msa.fa  -o msa.shrink.opt.fa  -u "-?XN"  -m  -v
```

As the option `-v` is set, the following information is printed:

```
reading MSA ... [ok]
processing MSA ......... [ok]
nrow=9  ncol=101  nusl=558
comparing sequences ......... [ok]
searching optimal skrinking ...
nrow=2  ncol=82   nusl=31   score=10.386  *
nrow=3  ncol=74   nusl=61   score=10.806  *
nrow=2  ncol=101  nusl=135  score=4.714
nrow=3  ncol=101  nusl=204  score=5.687
nrow=4  ncol=101  nusl=279  score=6.219
nrow=5  ncol=34   nusl=28   score=10.891  *
nrow=6  ncol=35   nusl=49   score=11.110  *
nrow=7  ncol=36   nusl=63   score=11.906  *
nrow=8  ncol=101  nusl=536  score=9.569
nrow=9  ncol=50   nusl=193  score=12.115  *
... [ok]
nrow=9  ncol=50   nusl=193
writing outfile ......... [ok]
```

The 'optimal' shrinked MSA contains the initial 9 sequences, but restricted to 50 aligned characters to reach a maximum score _s_&nbsp;=&nbsp;12.115. This shrinked MSA is written into the output file _msa.shrink.opt.fa_:

```
0 ACGTACGTACTGTACGTAATGTAC-----A---------CCAGTGC----
1 ACGTACGTACTG-ACGTAATGTACC----A---------CCAGTGC----
2 ACGTACGTAC---AC--AATGTACCC---A---------CCAG-------
3 ACGTACGTACTGTACGTAATGTACCCC--A---------CCAGTGC----
4 -------TACTGT---------ACCCCC-A---------CCAGTGCTAGT
5 ACGTA-----------T????TACCCCCCA-ATAA----CCAG-------
6 ---------------------TACCCCCCATATAATTTTCCAGTGCTAGT
7 NNNNNNNNNCTG------------------TATAATTTT-------TAGT
8 ACGTANNNNCNNNNNNTAATGTACCCCCCATATAATTTTCCAGTGCTAGT
```




## References

Chu PC, Beasley JE (1998) _A Genetic Algorithm for the Multidimensional Knapsack Problem_. **Journal of Heuristics**, 4(1):63-86. [doi:10.1023/A:1009642405419](https://doi.org/10.1023/A:1009642405419)

Laabadi S, Naimi M, El Amri H, Achchab B (2018) _The 0/1 Multidimensional Knapsack Problem and Its Variants: A Survey of Practical Models and Heuristic Approaches_. **American Journal of Operations Research**, 8:395-439. [doi:10.4236/ajor.2018.85023](https://doi.org/10.4236/ajor.2018.85023)

Weingartner HM, Ness DN (1967) _Methods for the Solution of the Multidimensional 0/1 Knapsack Problem_. **Operations Research**, 15(1):83-103. [doi:10.1287/opre.15.1.83](https://doi.org/10.1287/opre.15.1.83)
